#include <thread>
#include <Windows.h>
#include <stdio.h>
#include <process.h>
#include <iostream>
#include <cstring>

namespace os
{ 
	class thread : public std::thread
	{
	  public:
		thread() {}
		static void set_priority(std::thread &th, int priority) {
			if(SetThreadPriority(th.native_handle(), priority)) {
				std::cerr << "Failed os::thread::set_priority : " << std::strerror(errno) << std::endl;
			}
		}
	};
}