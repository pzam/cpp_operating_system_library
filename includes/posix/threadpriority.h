#include <thread>
#include <sched.h>
#include <pthread.h>
#include <iostream>
#include <cstring>

namespace os
{ 
	class thread : public std::thread
	{
	  public:
		thread() {}
		void set_priority(std::thread &th, int priority) {
			sch_params.sched_priority = priority;
			if(pthread_setschedparam(th.native_handle(), SCHED_FIFO, &sch_params)) {
				std::cerr << "Failed os::thread::set_priority : " << std::strerror(errno) << std::endl;
			}
		}
        private:
		sched_param sch_params; 
	};
}